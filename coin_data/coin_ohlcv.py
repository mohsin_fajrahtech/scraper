# CRYPTOCOMPARE - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
import psycopg2
from time import sleep



AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_KEY = 'a9de62dd0a4b454d2c5d1acbed37059bdab310a3a3a5a05f93a5f7babe4977f0'


folderpath = 'cryptocompare/'+datetime.date.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))

log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0
last_updated_dic = {}
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")

def calculate_limit(last_updated_timestamp):
    current_date = datetime.datetime.now()
    diff = current_date - last_updated_timestamp
    days, seconds = diff.days, diff.seconds
    hours = days * 24 + seconds // 3600
    return hours


def get_last_updated():
    global last_updated_dic
    global conn
    cur = conn.cursor()
    cur.execute('select coin,max(last_updated) from coin_data_ohlcv group by coin')
    for row in cur:
        last_updated_dic[row[0]] = calculate_limit(row[1]) - 1
    cur.close()

def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("coin_data_ohlcv","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'coin_data_ohlcv' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()

def load_data_coin_ohlcv_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    # conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()
    copy = "COPY coin_data_ohlcv(epoch,stamp,coin,close,high,low,open,volumefrom,volumeto) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()
    conn.close()

def load_data_db(filepath):
    global conn
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
    cur = conn.cursor()


    copy = "COPY coin_data(price,volume_24h,percent_change_1h, percent_change_24h, percent_change_7d, market_cap, last_updated, open, high, low, close, volume, coin, circulating_supply, total_supply, max_supply, date_added, num_market_pairs, cmc_rank) FROM STDIN with csv"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def upload_file(filenamepath):

    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)



# log_step("start")
# get_last_updated()

# print(last_updated_dic)
#print.test("test")




coins = ['BTC','ETH','XRP','BCH','EOS','XLM','LTC',
'USDT','ADA','XMR','MIOTA','TRX','ETC','DASH','NEO','BNB','XEM','XTZ','VET','ZEC','OMG','LSK','ZRX','QTUM','ONT','BTG',
'DCR','BCN','MKR','DOGE','BTS','ZIL','DGB','ICX','STEEM','NANO','AE','WAVES','BAT','REP','SC','MOAC','XVG','PPT','BTM',
'BCD','NPXS','RHOC','GNT','STRAT','ETP','HC','KCS','MITH','KMD','DCN','LINK','IOST','WTC','MAID','ARDR','DGD',
'HT','MONA','XIN','AION','CNX','ZEN','WAN','ELF','FUN','GXS','BNT','NAS','KIN','BTCP','HOT','RDD','EMC','POWR',
'XZC','MANA','ELA','ARK','MCO','KNC','NXT','TUSD','WAX','PAY','PIVX','AOA','QASH','VERI','CTXC','LRC','NULS','CMT']

# testcoins = ['BTC', 'ETH', 'ETC', 'DASH']

#1000 coins
start = 1
interval = 100

total_coin_list = []

for x in range(3):
    try:
        url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=505b1c4f-f05a-43d5-8a7a-348d72381790&sort=market_cap&start={}&limit={}&sort_dir=desc".format(start,interval)
        get_top_coins = requests.get(url)
        print(url)
        top_coin_response = get_top_coins.json()['data']
        #print(top_coin_response)
        for coin in top_coin_response:
            total_coin_list.append(coin['symbol'])
        #sleep(10)
    except Exception as ex:
        exception_text += str(ex)
        continue


    start = start + interval

coin_ohlcv_dict = { 
                        "epoch":[], \
                        "stamp":[], \
                        "coin" : [], \
                        "close":[], \
                        "high":[], \
                        "low":[], \
                        "open":[], \
                        "volumefrom":[], \
                        "volumeto":[], \
                    }

# for coin_t in total_coin_list:
# total_coin_list = ['BTC','ETH','LTC']
total_coin_list = ['BTC']
# total_coin_list = ['ETH']
# total_coin_list = ['LTC']

coin_list_id = {'BTC':1182, 'ETH': 7605, 'LTC': 3808}
limit = 2000
toTs = 1623401516 # change to current time when you want to run the backfill current_time
minTs = 1546300800 # # last backfill date 2019-01-01(1546300800) => 2021-06-01(1622505600)

for coin_t in total_coin_list:    
    print(coin_t)
    toTSLocal = toTs

    # #total_coin_list.append(coin['symbol'])
    # if coin_t in last_updated_dic.keys():
    #     coin_limit = last_updated_dic[coin_t]
    # #print(coin_t + " " + str(coin_limit))
    while toTSLocal > minTs:   
        url = "https://min-api.cryptocompare.com/data/histohour?fsym={}&tsym=USD&coinId={}&toTs={}&limit={}&api_key={}".format(coin_t,coin_list_id[coin_t],toTSLocal,limit,ACCESS_KEY)
        print(url)
        response_data = requests.get(url)
        #print(response_data.json())

        for response_data_t in response_data.json()['Data']:
            row_count += 1
            coin_ohlcv_dict['epoch'].append(response_data_t['time'])
            coin_ohlcv_dict['stamp'].append(dt.fromtimestamp(response_data_t['time']).strftime('%Y-%m-%d %H:%M:%S'))
            coin_ohlcv_dict['coin'].append(coin_t)
            coin_ohlcv_dict['close'].append(response_data_t['close'])
            coin_ohlcv_dict['high'].append(response_data_t['high'])
            coin_ohlcv_dict['low'].append(response_data_t['low'])
            coin_ohlcv_dict['open'].append(response_data_t['open'])
            coin_ohlcv_dict['volumefrom'].append(response_data_t['volumefrom'])
            coin_ohlcv_dict['volumeto'].append(response_data_t['volumeto'])

        toTSLocal = min(coin_ohlcv_dict['epoch'])

#        print(dt.fromtimestamp(1549155600))
#        print(response_data_t)

#        break
#    print(coin_ohlcv_dict)

    #break
topics_data = pd.DataFrame(coin_ohlcv_dict)
if not os.path.exists('coin_ohlcv'):
    os.makedirs('coin_ohlcv')
path = os.path.join(os.getcwd() +'/coin_ohlcv/'+filename+'.csv')
topics_data.to_csv(path,index=False)
# upload_file(path)
load_data_coin_ohlcv_db(path)
# log_step("end")
#https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=10




# try:
#     print('CSV file '+filename+' is going to be uploaded')
#     upload_file(folderpath+'/'+ filename +'.csv')
#     print('CSV file '+filename+' is uploaded')
#     load_data_db(folderpath+'/'+ filename +'.csv')
#     print('coin_date table is updated')
#     log_step("end")

# except Exception as ex:
#     print("Exception Occurred :" + str(ex))
#     exception_text += str(ex)
#     log_step("error")
