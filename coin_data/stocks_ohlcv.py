# CRYPTOCOMPARE - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
import psycopg2
from time import sleep
from config_sp500 import STOCKS



AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_TOKEN = "869161763534770176-y7sMuXdSR3fjXuhclJRJfm5uTao2kPw"
ACCESS_TOKEN_SECRET = "Nw13OMcxT9MGRk2fChlqIYHSQxxGiTn6CVRHemfjSZdYk"
folderpath = 'coinmarketcap/stocks/'+datetime.date.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))

#AlphaAdvantageAPI Parameters
API_FUNCTION = 'TIME_SERIES_INTRADAY'
API_SMA_FUNCTION = 'SMA'
API_EMA_FUNCTION = 'EMA'
API_MACD_FUNCTION = 'MACD'
API_RSI_FUNCTION = 'RSI'
API_BBANDS_FUNCTION = 'BBANDS'
API_ADX_FUNCTION = 'ADX'

TIME_PERIOD_FOR_HIGH_INDICATOR = 15
SERIES_TYPE = 'close'
STOCK = ''
INTERVAL = '15min'
API_KEY = '0VVS08FG6SF33CKT'


conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0
last_execution_timestamp = ""

def get_last_execution_timestamp(stock):
    global last_execution_timestamp

    cur = conn.cursor()
    cur.execute('select max(last_updated) from stock_ohlcv_data where stock = %s', (stock,))
    for row in cur:
        last_execution_timestamp = row[0]
    cur.close()

    if (last_execution_timestamp == None):
        last_execution_timestamp = "2019-01-01 00:00:00"
    

def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("stock_ohlcv_data","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'stock_ohlcv_data' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()


def load_data_db(filepath):
    global conn
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
    cur = conn.cursor()


    copy = "COPY stock_ohlcv_data(open, high, low, close, volume, sma_close, ema_close, macd_close, rsi_close, bbands_lower_close, bbands_middle_close, bbands_upper_close, adx_close, last_updated, stock, stock_full_name) FROM STDIN with csv"

    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def upload_file(filenamepath):
    global ACCESS_TOKEN
    global ACCESS_TOKEN_SECRET
    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)

def get_stock_data(STOCK, STOCK_FULL_NAME):
    global row_count
    global exception_text
    global last_execution_timestamp

    get_last_execution_timestamp(STOCK)

    print(last_execution_timestamp)

    if(not isinstance(last_execution_timestamp, datetime.datetime)):
        last_execution_timestamp = dt.strptime(last_execution_timestamp,"%Y-%m-%d %H:%M:%S")

    filename = STOCK
    stock_dict = {}
    record_flag = 0
    delay_timer = 10

    try:

        stock_real_time_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&apikey={}'\
                .format(API_FUNCTION, STOCK, INTERVAL, API_KEY)
        print(stock_real_time_api)
        stock_real_time_api_response = requests.get(stock_real_time_api)
        stock_real_time_api_response_json = stock_real_time_api_response.json()

        stock_sma_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_SMA_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_sma_api_response = requests.get(stock_sma_api)
        stock_sma_api_response_json = stock_sma_api_response.json()
        print(stock_sma_api)

        stock_ema_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_EMA_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_ema_api_response = requests.get(stock_ema_api)
        stock_ema_api_response_json = stock_ema_api_response.json()
        print(stock_ema_api)

        stock_macd_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_MACD_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_macd_api_response = requests.get(stock_macd_api)
        stock_macd_api_response_json = stock_macd_api_response.json()
        print(stock_macd_api)

        stock_rsi_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_RSI_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_rsi_api_response = requests.get(stock_rsi_api)
        stock_rsi_api_response_json = stock_rsi_api_response.json()   
        print(stock_rsi_api)

        stock_bbands_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_BBANDS_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_bbands_api_response = requests.get(stock_bbands_api)
        stock_bbands_api_response_json = stock_bbands_api_response.json()   
        print(stock_bbands_api)

        stock_adx_api = 'https://www.alphavantage.co/query?function={}&symbol={}&interval={}&time_period={}&series_type={}&apikey={}'\
                .format(API_ADX_FUNCTION, STOCK, INTERVAL, TIME_PERIOD_FOR_HIGH_INDICATOR, SERIES_TYPE, API_KEY)
        stock_adx_api_response = requests.get(stock_adx_api)
        stock_adx_api_response_json = stock_adx_api_response.json()  
        print(stock_adx_api)


        del stock_real_time_api_response_json["Meta Data"]
        del stock_sma_api_response_json["Meta Data"]
        del stock_ema_api_response_json["Meta Data"]
        del stock_macd_api_response_json["Meta Data"]
        del stock_rsi_api_response_json["Meta Data"]
        del stock_bbands_api_response_json["Meta Data"]
        del stock_adx_api_response_json["Meta Data"]

        stock_real_time_api_response_json = stock_real_time_api_response_json["Time Series (15min)"]
        stock_sma_api_response_json = stock_sma_api_response_json["Technical Analysis: SMA"]
        stock_ema_api_response_json = stock_ema_api_response_json["Technical Analysis: EMA"]  
        stock_macd_api_response_json = stock_macd_api_response_json["Technical Analysis: MACD"]
        stock_rsi_api_response_json = stock_rsi_api_response_json["Technical Analysis: RSI"]
        stock_bbands_api_response_json = stock_bbands_api_response_json["Technical Analysis: BBANDS"]
        stock_adx_api_response_json = stock_adx_api_response_json["Technical Analysis: ADX"]


        if not os.path.exists(folderpath):
            os.makedirs(folderpath)

        for key, value in stock_real_time_api_response_json.items():
            stock_dict = {}
            if(last_execution_timestamp < dt.strptime(key,"%Y-%m-%d %H:%M:%S")):
                record_flag = 1
                row_count += 1
                stock_dict['open'] = value['1. open']
                stock_dict['high'] = value['2. high']
                stock_dict['low'] = value['3. low']
                stock_dict['close'] = value['4. close']
                stock_dict['volume'] = value['5. volume']
                if key[:-3] in stock_sma_api_response_json:
                    stock_dict['sma_close'] = stock_sma_api_response_json[key[:-3]]['SMA']
                else:
                    stock_dict['sma_close'] = None
                if key[:-3] in stock_ema_api_response_json:
                    stock_dict['ema_close'] = stock_ema_api_response_json[key[:-3]]['EMA']
                else:
                    stock_dict['ema_close'] = None
                if key[:-3] in stock_macd_api_response_json:
                    stock_dict['macd_close'] = stock_macd_api_response_json[key[:-3]]['MACD']
                else:
                    stock_dict['macd_close'] = None
                if key[:-3] in stock_rsi_api_response_json:
                    stock_dict['rsi_close'] = stock_rsi_api_response_json[key[:-3]]['RSI']
                else:
                    stock_dict['rsi_close'] = None
                if key[:-3] in stock_bbands_api_response_json:
                    stock_dict['bbands_lower_close'] = stock_bbands_api_response_json[key[:-3]]['Real Lower Band']
                    stock_dict['bbands_middle_close'] = stock_bbands_api_response_json[key[:-3]]['Real Middle Band']
                    stock_dict['bbands_upper_close'] = stock_bbands_api_response_json[key[:-3]]['Real Upper Band']
                else:
                    stock_dict['bbands_lower_close'] = None
                    stock_dict['bbands_middle_close'] = None
                    stock_dict['bbands_upper_close'] = None
                if key[:-3] in stock_adx_api_response_json:
                    stock_dict['adx_close'] = stock_adx_api_response_json[key[:-3]]['ADX']
                else:
                    stock_dict['adx_close'] = None
                stock_dict['last_updated'] = key
                stock_dict['stock'] = STOCK
                stock_dict['stock_full_name'] = STOCK_FULL_NAME
                df = pd.DataFrame(stock_dict, index=['last_updated'])
                df.to_csv(path_or_buf = folderpath+'/'+ filename +'.csv', mode='a',header=False, sep=',', index=False,encoding="utf-16") 

        if(record_flag == 1): 
            print('CSV file '+filename+' is going to be uploaded')
            upload_file(folderpath+'/'+ filename +'.csv')
            print('CSV file '+filename+' is uploaded')
            load_data_db(folderpath+'/'+ filename +'.csv')
            print('stocks table is updated')
        else:
            print("No new data for stock " + STOCK)

    except Exception as ex:
        print(str(ex))
        print(STOCK + " Had issues")
        sleep(10)


log_step("start")

for key, value in STOCKS.items():
    get_stock_data(key, value)
    sleep(20)

try:
    log_step("end")

except Exception as ex:
    print("Exception Occurred :" + str(ex))
    exception_text += str(ex)
    log_step("error")
