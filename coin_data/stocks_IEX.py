# IEX - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
from datetime import timedelta
import psycopg2
from time import sleep
from config_sp500 import STOCKS
from datetime import datetime
from iexfinance.stocks import get_historical_intraday



AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_TOKEN = "869161763534770176-y7sMuXdSR3fjXuhclJRJfm5uTao2kPw"
ACCESS_TOKEN_SECRET = "Nw13OMcxT9MGRk2fChlqIYHSQxxGiTn6CVRHemfjSZdYk"
folderpath = 'coinmarketcap/stocks/'+dt.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))

API_KEY = 'sk_d468e02ba8dd4116882c516edf371c3d'


conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0
last_execution_timestamp = ""
current_date = dt.now()

def get_last_execution_timestamp(stock):
    global last_execution_timestamp

    cur = conn.cursor()
    cur.execute('select max(last_updated) from stock_ohlcv_data where stock = %s', (stock,))
    for row in cur:
        last_execution_timestamp = row[0]
    cur.close()

    if (last_execution_timestamp == None):
        last_execution_timestamp = "2019-01-01 00:00:00"
    

def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("stock_ohlcv_data","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'stock_ohlcv_data' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()


def load_data_db(filepath):
    global conn
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
    cur = conn.cursor()


    copy = "COPY stock_ohlcv_data(last_updated, close, high, low, open, stock, stock_full_name) FROM STDIN with csv"

    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def upload_file(filenamepath):
    global ACCESS_TOKEN
    global ACCESS_TOKEN_SECRET
    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)

def get_stock_data(STOCK, STOCK_FULL_NAME):
    global row_count
    global exception_text
    global last_execution_timestamp

    get_last_execution_timestamp(STOCK)

    filename = STOCK
    stock_dict = {}
    record_flag = 0
    delay_timer = 10

    if not os.path.exists(folderpath):
        os.makedirs(folderpath)

    try:

        date = last_execution_timestamp + timedelta(days=1)
        start_date = dt(date.year, date.month, date.day)
        print("we are going to scrap data from: " + str(start_date))
        print("stock is: " + STOCK)

        end_date = dt(current_date.year, current_date.month, current_date.day)
        print("we are going to scrap data till: " + str(end_date))

        while(start_date < end_date):
            print("we are going to scrap data of day: " + str(start_date))
            df = get_historical_intraday(STOCK, start_date, token="sk_d468e02ba8dd4116882c516edf371c3d", output_format='pandas')
            if(not df.empty):

                df = df[['close', 'high', 'low', 'open']]
                last_row = df.iloc[-1:]
                last_row.index = last_row.index + pd.Timedelta(minutes=1)
                df = df.append(last_row)
                final_df = df.resample('15Min').mean()
                print(final_df)
                final_df['stock'] = STOCK
                final_df['stock_full_name'] = STOCK_FULL_NAME

                print("after filtering the dataframe is")
                print(final_df)

                row_count = row_count + len(final_df.index)
                print("total number of records processed")
                print(row_count)

                final_df.to_csv(path_or_buf = folderpath+'/'+ filename +'.csv', mode='a',header=False, sep=',',encoding="utf-16")
                start_date = start_date + timedelta(days=1)
                print('CSV file '+filename+' is going to be uploaded')
                upload_file(folderpath+'/'+ filename +'.csv')
                print('CSV file '+filename+' is uploaded')
                load_data_db(folderpath+'/'+ filename +'.csv')
                print('stocks table is updated')
                start_date = start_date + timedelta(days=1)
            else:
                print("looks like it is weekend!! NO DATA")
                start_date = start_date + timedelta(days=1)
    except Exception as ex:
        print(ex)
        print("stock: " + STOCK + " had issues")
    

log_step("start")

for key, value in STOCKS.items():
    get_stock_data(key, value)
    sleep(1)

try:
    log_step("end")

except Exception as ex:
    print("Exception Occurred :" + str(ex))
    exception_text += str(ex)
    log_step("error")
