# CRYPTOCOMPARE - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
import psycopg2
from time import sleep
from sqlalchemy import create_engine


AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_KEY = 'a9de62dd0a4b454d2c5d1acbed37059bdab310a3a3a5a05f93a5f7babe4977f0'


folderpath = 'cryptocompare/'+datetime.date.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))
#conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0



def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("coin_data","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'coin_data' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()

def load_data_coin_ohlcv_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()
    copy = "COPY coin_data_ohlcv(last_updated,coin,close,high,low,open,volumefrom,volumeto) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()
    conn.close()

def load_data_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()


    copy = "COPY coin_social(epoch,stamp,coin,coin_id,comments,posts,followers,points,overview_page_views,analysis_page_views,markets_page_views,charts_page_views,trades_page_views,forum_page_views,influence_page_views,total_page_views,fb_likes,fb_talking_about,twitter_followers,twitter_following,twitter_lists,twitter_favourites,twitter_statuses,reddit_subscribers,reddit_active_users,reddit_posts_per_hour,reddit_posts_per_day,reddit_comments_per_hour,reddit_comments_per_day,code_repo_stars,code_repo_forks,code_repo_subscribers,code_repo_open_pull_issues,code_repo_closed_pull_issues,code_repo_open_issues,code_repo_closed_issues) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def upload_file(filenamepath):

    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)



# coins = ['BTC','ETH','XRP','BCH','EOS','XLM','LTC',
# 'USDT','ADA','XMR','MIOTA','TRX','ETC','DASH','NEO','BNB','XEM','XTZ','VET','ZEC','OMG','LSK','ZRX','QTUM','ONT','BTG',
# 'DCR','BCN','MKR','DOGE','BTS','ZIL','DGB','ICX','STEEM','NANO','AE','WAVES','BAT','REP','SC','MOAC','XVG','PPT','BTM',
# 'BCD','NPXS','RHOC','GNT','STRAT','ETP','HC','KCS','MITH','KMD','DCN','LINK','IOST','WTC','MAID','ARDR','DGD',
# 'HT','MONA','XIN','AION','CNX','ZEN','WAN','ELF','FUN','GXS','BNT','NAS','KIN','BTCP','HOT','RDD','EMC','POWR',
# 'XZC','MANA','ELA','ARK','MCO','KNC','NXT','TUSD','WAX','PAY','PIVX','AOA','QASH','VERI','CTXC','LRC','NULS','CMT']

# testcoins = ['BTC', 'ETH', 'ETC', 'DASH']
#log_step("start")

#1000 coins
# start = 1
# interval = 100

# total_coin_list = []

# for x in range(1):
#     try:
#         url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=505b1c4f-f05a-43d5-8a7a-348d72381790&sort=market_cap&start={}&limit={}&sort_dir=desc".format(start,interval)
#         get_top_coins = requests.get(url)
#         print(url)
#         top_coin_response = get_top_coins.json()['data']
#         #print(top_coin_response)
#         for coin in top_coin_response:
#             total_coin_list.append(coin['symbol'])
#         #sleep(10)
#     except Exception as ex:
#         exception_text += str(ex)
#         continue


#     start = start + interval

# print(total_coin_list)


# url = "https://min-api.cryptocompare.com/data/all/coinlist"
# response_data = requests.get(url,headers={"apikey":"f9cd060c3d51269f95f8aee95c7538f3d07a7f650cae3462431acbd1618098b1"})

# coin_list_id = response_data.json()['Data']

#print(coin_list_id.test)

coin_social_data_dict = { 
                        "epoch": [], \
                        "stamp":[], \
                        "coin":[], \
                        "coin_id":[], \
                        "comments":[], \
                        "posts":[], \
                        "followers":[], \
                        "points":[], \
                        "overview_page_views":[], \
                        "analysis_page_views":[], \
                        "markets_page_views":[], \
                        "charts_page_views":[], \
                        "trades_page_views":[], \
                        "forum_page_views":[], \
                        "influence_page_views":[], \
                        "total_page_views":[], \
                        "fb_likes":[], \
                        "fb_talking_about":[], \
                        "twitter_followers":[], \
                        "twitter_following":[], \
                        "twitter_lists":[], \
                        "twitter_favourites":[], \
                        "twitter_statuses":[], \
                        "reddit_subscribers":[], \
                        "reddit_active_users":[], \
                        "reddit_posts_per_hour":[], \
                        "reddit_posts_per_day":[], \
                        "reddit_comments_per_hour":[], \
                        "reddit_comments_per_day":[], \
                        "code_repo_stars":[], \
                        "code_repo_forks" : [], \
                        "code_repo_subscribers":[], \
                        "code_repo_open_pull_issues":[], \
                        "code_repo_closed_pull_issues":[], \
                        "code_repo_open_issues":[], \
                        "code_repo_closed_issues":[], \
                    }

# total_coin_list = ['BTC','ETH','LTC']
# total_coin_list = ['BTC']
# total_coin_list = ['ETH']
total_coin_list = ['LTC']

coin_list_id = {'BTC':1182, 'ETH': 7605, 'LTC': 3808}
limit = 2000
toTs = 1623401516 # change to current time when you want to run the backfill current_time
minTs = 1546300800 # # last backfill date 2019-01-01(1546300800) => 2021-06-01(1622505600)
for coin_t in total_coin_list:
    print(coin_t)
    toTSLocal = toTs

    while toTSLocal > minTs:
    #total_coin_list.append(coin['symbol'])
        url = "https://min-api.cryptocompare.com/data/social/coin/histo/hour?coinId={}&toTs={}&limit={}&api_key={}".format(coin_list_id[coin_t],toTSLocal,limit,ACCESS_KEY)
        print(url)
        response_data = requests.get(url)

        print(coin_list_id[coin_t])
        for response_data_t in response_data.json()['Data']:
            # print(response_data_t['time'])
    #        print(dt.fromtimestamp(response_data_t['time']).strftime('%Y-%m-%d %H:%M:%S'))
            # print(dt.fromtimestamp(response_data_t['time']).strftime('%Y-%m-%d %H:%M:%S'))
            coin_social_data_dict['epoch'].append(response_data_t['time'])
            coin_social_data_dict['stamp'].append(dt.fromtimestamp(response_data_t['time']).strftime('%Y-%m-%d %H:%M:%S'))
            coin_social_data_dict['coin'].append(coin_t)
            coin_social_data_dict['coin_id'].append(coin_list_id[coin_t])
            coin_social_data_dict['comments'].append(response_data_t['comments'])
            coin_social_data_dict['posts'].append(response_data_t['posts'])
            coin_social_data_dict['followers'].append(response_data_t['followers'])
            coin_social_data_dict['points'].append(response_data_t['points'])
            coin_social_data_dict['overview_page_views'].append(response_data_t['overview_page_views'])
            coin_social_data_dict['analysis_page_views'].append(response_data_t['analysis_page_views'])
            coin_social_data_dict['markets_page_views'].append(response_data_t['markets_page_views'])
            coin_social_data_dict['charts_page_views'].append(response_data_t['charts_page_views'])
            coin_social_data_dict['trades_page_views'].append(response_data_t['trades_page_views'])
            coin_social_data_dict['forum_page_views'].append(response_data_t['forum_page_views'])
            coin_social_data_dict['influence_page_views'].append(response_data_t['influence_page_views'])
            coin_social_data_dict['total_page_views'].append(response_data_t['total_page_views'])
            coin_social_data_dict['fb_likes'].append(response_data_t['fb_likes'])
            coin_social_data_dict['fb_talking_about'].append(response_data_t['fb_talking_about'])
            coin_social_data_dict['twitter_followers'].append(response_data_t['twitter_followers'])
            coin_social_data_dict['twitter_following'].append(response_data_t['twitter_following'])
            coin_social_data_dict['twitter_lists'].append(response_data_t['twitter_lists'])
            coin_social_data_dict['twitter_favourites'].append(response_data_t['twitter_favourites'])
            coin_social_data_dict['twitter_statuses'].append(response_data_t['twitter_statuses'])
            coin_social_data_dict['reddit_subscribers'].append(response_data_t['reddit_subscribers'])
            coin_social_data_dict['reddit_active_users'].append(response_data_t['reddit_active_users'])
            coin_social_data_dict['reddit_posts_per_hour'].append(response_data_t['reddit_posts_per_hour'])
            coin_social_data_dict['reddit_posts_per_day'].append(response_data_t['reddit_posts_per_day'])
            coin_social_data_dict['reddit_comments_per_hour'].append(response_data_t['reddit_comments_per_hour'])
            coin_social_data_dict['reddit_comments_per_day'].append(response_data_t['reddit_comments_per_day'])
            coin_social_data_dict['code_repo_stars'].append(response_data_t['code_repo_stars'])
            coin_social_data_dict['code_repo_forks'].append(response_data_t['code_repo_forks'])
            coin_social_data_dict['code_repo_subscribers'].append(response_data_t['code_repo_subscribers'])
            coin_social_data_dict['code_repo_open_pull_issues'].append(response_data_t['code_repo_open_pull_issues'])
            coin_social_data_dict['code_repo_closed_pull_issues'].append(response_data_t['code_repo_closed_pull_issues'])
            coin_social_data_dict['code_repo_open_issues'].append(response_data_t['code_repo_open_issues'])
            coin_social_data_dict['code_repo_closed_issues'].append(response_data_t['code_repo_closed_issues'])

        toTSLocal = min(coin_social_data_dict['epoch'])
#        print(dt.fromtimestamp(1549155600))
#        print(response_data_t)

#        break
#    print(coin_ohlcv_dict)

    #break
topics_data = pd.DataFrame(coin_social_data_dict)
# print(coin_social_data_dict)
if not os.path.exists('coin_social_data'):
    os.makedirs('coin_social_data')
path = os.path.join(os.getcwd() +'/coin_social_data/'+filename+'.csv')
topics_data.to_csv(path,index=False)
load_data_db(path)




# try:
#     print('CSV file '+filename+' is going to be uploaded')
#     upload_file(folderpath+'/'+ filename +'.csv')
#     print('CSV file '+filename+' is uploaded')
#     load_data_db(folderpath+'/'+ filename +'.csv')
#     print('coin_date table is updated')
#     log_step("end")

# except Exception as ex:
#     print("Exception Occurred :" + str(ex))
#     exception_text += str(ex)
#     log_step("error")
