# CRYPTOCOMPARE - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
import psycopg2
from time import sleep



AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_TOKEN = "869161763534770176-y7sMuXdSR3fjXuhclJRJfm5uTao2kPw"
ACCESS_TOKEN_SECRET = "Nw13OMcxT9MGRk2fChlqIYHSQxxGiTn6CVRHemfjSZdYk"
folderpath = 'coinmarketcap/'+datetime.date.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0


def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("coin_data","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'coin_data' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()


def load_data_db(filepath):
    global conn
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
    cur = conn.cursor()


    copy = "COPY coin_data(price,volume_24h,percent_change_1h, percent_change_24h, percent_change_7d, market_cap, last_updated, open, high, low, close, volume, coin, circulating_supply, total_supply, max_supply, date_added, num_market_pairs, cmc_rank) FROM STDIN with csv"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def upload_file(filenamepath):
    global ACCESS_TOKEN
    global ACCESS_TOKEN_SECRET
    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)

def get_coin_data(coinlist):
    global row_count
    global exception_text

    if ',0xBTC' in coinlist:
        coinlist = coinlist.replace(',0xBTC','')

    url_marketcap = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol={}&CMC_PRO_API_KEY=4c2df797-1ed0-4535-86fb-5f5037dc0d39'\
            .format(coinlist)
    page_marketcap = requests.get(url_marketcap)
    print (url_marketcap)

    url_fluctuation = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/ohlcv/latest?symbol={}&CMC_PRO_API_KEY=4c2df797-1ed0-4535-86fb-5f5037dc0d39'\
            .format(coinlist)
    page_fluctuation = requests.get(url_fluctuation)

    print (url_fluctuation)

    coin_list = coinlist.split(',')

    for coin in coin_list:
        row_count += 1
        try: 
            data_marketcap = page_marketcap.json()['data'][coin]['quote']['USD']
            data_fluctuation = page_fluctuation.json()['data'][coin]['quote']['USD']
            coinmarketcap_dict = Merge(data_marketcap, data_fluctuation)
            coinmarketcap_dict['coin'] = coin
            coinmarketcap_dict['circulating_supply'] = page_marketcap.json()['data'][coin]['circulating_supply']
            coinmarketcap_dict['total_supply'] = page_marketcap.json()['data'][coin]['total_supply']
            coinmarketcap_dict['max_supply'] = page_marketcap.json()['data'][coin]['max_supply']
            coinmarketcap_dict['date_added'] = page_marketcap.json()['data'][coin]['date_added']
            coinmarketcap_dict['num_market_pairs'] = page_marketcap.json()['data'][coin]['num_market_pairs']
            coinmarketcap_dict['cmc_rank'] = page_marketcap.json()['data'][coin]['cmc_rank']

            df = pd.DataFrame(coinmarketcap_dict, index=['last_updated'])
            if not os.path.exists(folderpath):
                os.makedirs(folderpath)
            df.to_csv(path_or_buf=folderpath+'/'+ filename +'.csv', mode='a',header=False, sep=',', index=False,encoding="utf-16")        
            print('API Response of '+coin+' has been received')
        except Exception as ex:
            print("Exception Occurred in" + coin)
            print(str(ex))
            exception_text += str(ex)


coins = ['BTC','ETH','XRP','BCH','EOS','XLM','LTC',
'USDT','ADA','XMR','MIOTA','TRX','ETC','DASH','NEO','BNB','XEM','XTZ','VET','ZEC','OMG','LSK','ZRX','QTUM','ONT','BTG',
'DCR','BCN','MKR','DOGE','BTS','ZIL','DGB','ICX','STEEM','NANO','AE','WAVES','BAT','REP','SC','MOAC','XVG','PPT','BTM',
'BCD','NPXS','RHOC','GNT','STRAT','ETP','HC','KCS','MITH','KMD','DCN','LINK','IOST','WTC','MAID','ARDR','DGD',
'HT','MONA','XIN','AION','CNX','ZEN','WAN','ELF','FUN','GXS','BNT','NAS','KIN','BTCP','HOT','RDD','EMC','POWR',
'XZC','MANA','ELA','ARK','MCO','KNC','NXT','TUSD','WAX','PAY','PIVX','AOA','QASH','VERI','CTXC','LRC','NULS','CMT']

testcoins = ['BTC', 'ETH', 'ETC', 'DASH']
log_step("start")

#1000 coins
start = 1
interval = 100


for x in range(7):
    try:
        url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=4c2df797-1ed0-4535-86fb-5f5037dc0d39&sort=market_cap&start={}&limit={}&sort_dir=desc".format(start,interval)
        get_top_coins = requests.get(url)
        print(url)
        top_coin_response = get_top_coins.json()['data']
        sleep(10)
    except Exception as ex:
        exception_text += str(ex)
        continue

    with open('topcoins.txt', 'w') as f:
        for coin in top_coin_response:
            f.write("%s" % coin['symbol'] + ',')



    with open('topcoins.txt', 'rb+') as filehandle:
        filehandle.seek(-1, os.SEEK_END)
        filehandle.truncate()


    with open('topcoins.txt', 'r') as myfile:
        comma_seperated_coins = myfile.read()

    get_coin_data(comma_seperated_coins)

    start = start + interval



try:
    print('CSV file '+filename+' is going to be uploaded')
    upload_file(folderpath+'/'+ filename +'.csv')
    print('CSV file '+filename+' is uploaded')
    load_data_db(folderpath+'/'+ filename +'.csv')
    print('coin_date table is updated')
    log_step("end")

except Exception as ex:
    print("Exception Occurred :" + str(ex))
    exception_text += str(ex)
    log_step("error")
