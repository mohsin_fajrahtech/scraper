# CRYPTOCOMPARE - Fetching coin data of the last 24h

import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
import psycopg2
from time import sleep



AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
ACCESS_TOKEN = "869161763534770176-y7sMuXdSR3fjXuhclJRJfm5uTao2kPw"
ACCESS_TOKEN_SECRET = "Nw13OMcxT9MGRk2fChlqIYHSQxxGiTn6CVRHemfjSZdYk"
folderpath = 'coinmarketcap_totalmarketcap/'+datetime.date.today().strftime("%Y-%m-%d")
filename = str(dt.now().strftime('%Y%m%d%H%M%S'))
execution_date = str(dt.now().strftime('%Y-%m-%d'))
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 1
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0


def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.now()
        cur.execute(start_sql, ("total_market_cap","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'total_market_cap' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])
        cur.close()


    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = 1, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = 1, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.now()
        row_count = 1
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()


def load_data_db(filepath):
    global conn
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
    cur = conn.cursor()


    copy = "COPY marketcaphistory(total_market_cap, total_volume_24h, last_updated, active_cryptocurrencies, active_market_pairs, active_exchanges, eth_dominance, btc_dominance) FROM STDIN with csv"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

def upload_file(filenamepath):
    global ACCESS_TOKEN
    global ACCESS_TOKEN_SECRET
    global BUCKET

    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    s3.upload_file(filenamepath, BUCKET, filenamepath)

def get_total_market_cap():
    global row_count

    url_fluctuation = 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest?CMC_PRO_API_KEY=4c2df797-1ed0-4535-86fb-5f5037dc0d39'
    page_fluctuation = requests.get(url_fluctuation)

    marketcap_dict = page_fluctuation.json()['data']['quote']['USD']
    marketcap_dict['active_cryptocurrencies'] = page_fluctuation.json()['data']['active_cryptocurrencies']
    marketcap_dict['active_market_pairs'] = page_fluctuation.json()['data']['active_market_pairs']
    marketcap_dict['active_exchanges'] = page_fluctuation.json()['data']['active_exchanges']
    marketcap_dict['eth_dominance'] = page_fluctuation.json()['data']['eth_dominance']
    marketcap_dict['btc_dominance'] = page_fluctuation.json()['data']['btc_dominance']

    print (marketcap_dict)


    df = pd.DataFrame(marketcap_dict, index=['last_updated'])
    if not os.path.exists(folderpath):
        os.makedirs(folderpath)
    df.to_csv(path_or_buf=folderpath+'/'+ filename +'.csv', mode='a',header=False, sep=',', index=False,encoding="utf-16")        
    print('API Response of '+url_fluctuation+' has been received')


log_step("start")
try:
    get_total_market_cap()
    print('CSV file '+filename+' is going to be uploaded')
    upload_file(folderpath+'/'+ filename +'.csv')
    print('CSV file '+filename+' is uploaded')
    load_data_db(folderpath+'/'+ filename +'.csv')
    print('marketcaphistory table is updated')
    log_step("end")

except Exception as ex:
    print("Exception Occurred :" + str(ex))
    exception_text += str(ex)
    log_step("error")
