import requests
import datetime
import pandas as pd
import os
import boto3
import time
from datetime import datetime as dt
from datetime import timedelta
import psycopg2
from textblob import TextBlob
from time import sleep

filename = str(dt.now().strftime('%Y%m%d%H%M%S'))

row_count = 0
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")

def load_data_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()
    copy = "COPY reddit_pushshift(reddit_name,epoch,stamp,coin,title,post_id,author,score,polarity,subjectivity,created) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()
    conn.close()

def Merge(dict1, dict2): 
    res = {**dict1, **dict2} 
    return res 

pushshift_dict = { 
                        "reddit_name":[], \
                        "epoch":[], \
                        "stamp":[], \
                        "coin" : [], \
                        "title":[], \
                        "post_id":[], \
                        "author":[], \
                        "score":[], \
                        "polarity":[], \
                        "subjectivity":[], \
                        "created":[], \
                    }

# total_coin_list = ['BTC']
# total_coin_list = ['ETH']
# total_coin_list = ['LTC']

coins = {
    'bitcoin' : 'BTC',
    'ethereum' : 'ETH',
    'litecoin': 'LTC'
}
coin = 'BTC'
subreddit = 'bitcoin'
size = 1000

# min_time = dt.strptime("2021-04-01 00:00:00", '%Y-%m-%d %H:%M:%S')
# max_time = dt.strptime("2021-05-01 00:00:00", '%Y-%m-%d %H:%M:%S')

# min_time = dt.strptime("2021-05-01 00:00:00", '%Y-%m-%d %H:%M:%S')
# max_time = dt.strptime("2021-06-01 00:00:00", '%Y-%m-%d %H:%M:%S')

# min_time = dt.strptime("2021-06-01 00:00:00", '%Y-%m-%d %H:%M:%S')
# max_time = dt.strptime("2021-06-20 00:00:00", '%Y-%m-%d %H:%M:%S')


min_time = dt.strptime("2021-04-01 00:00:00", '%Y-%m-%d %H:%M:%S')
max_time = dt.strptime("2021-06-20 00:00:00", '%Y-%m-%d %H:%M:%S')



def daterange(start_date, end_date):
    delta = timedelta(hours=1)
    while start_date < end_date:
        yield start_date
        start_date += delta

start_time = 0
end_time = 0
total_requests = 1919
request_made = 0
for index, single_timestamp in enumerate(daterange(min_time, max_time)):
    if index == 0:
        start_time = min_time.timestamp()
        continue
    else:
        end_time = single_timestamp.timestamp()
    
    # Make calls here
    url = "https://api.pushshift.io/reddit/search/submission/?subreddit={}&sort=asc&sort_type=created_utc&after={}&before={}&size={}".format(subreddit,int(start_time),int(end_time),size)
    # print(url)

    try:
        response_data = requests.get(url)
        for response_data_t in response_data.json()['data']:
            title_blob = TextBlob(response_data_t['title'])

            pushshift_dict['reddit_name'].append(subreddit)
            pushshift_dict['epoch'].append(int(start_time))
            pushshift_dict['stamp'].append(dt.fromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S'))
            pushshift_dict['coin'].append(coin)
            pushshift_dict['title'].append(response_data_t['title'])
            pushshift_dict['post_id'].append(response_data_t['id'])
            pushshift_dict['author'].append(response_data_t['author'])
            pushshift_dict['score'].append(response_data_t['score'])
            pushshift_dict['polarity'].append(title_blob.sentiment.polarity)
            pushshift_dict['subjectivity'].append(title_blob.sentiment.subjectivity)
            pushshift_dict['created'].append(dt.fromtimestamp(response_data_t['created_utc']).strftime('%Y-%m-%d %H:%M:%S'))
    except Exception as ex:
        print(ex)

    request_made += 1
    print(f'Processing {request_made} out of {total_requests}, %{request_made/total_requests*100} completed!')

    start_time = end_time

reddit_data = pd.DataFrame(pushshift_dict)

if not os.path.exists('pushshift_submissions'):
    os.makedirs('pushshift_submissions')
path = os.path.join(os.getcwd() +'/pushshift_submissions/'+filename+'.csv')
reddit_data.to_csv(path,index=False)
load_data_db(path)
