# importing the module
import praw

import swifter
import multiprocessing as mp

import numpy as np #scientific computing
import pandas as pd #data management

import psycopg2
from sqlalchemy import create_engine

# initialize with appropriate values
client_id = "p6lMFlg6H_slag"
client_secret = "MgWX58Ft7Yrms-porlFrjANPlqs"
username = "justforthescriptslol"
password = "justforthescriptslol"
user_agent = "ascript"

conn = create_engine('postgresql+psycopg2://root:root@0.0.0.0/coinmarket').connect()

# creating an authorized reddit instance
reddit = praw.Reddit(client_id = client_id, 
                     client_secret = client_secret, 
                     username = username, 
                     password = password,
                     user_agent = user_agent) 

# the name of the redditor
data = pd.read_csv('./reddit_authors.csv', sep='|')

def get_karma(redditor_name):
    try:
        redditor = reddit.redditor(redditor_name)
        karma = redditor.comment_karma
        print(f'Fetched redditor: {redditor_name}, got karma: {karma}')
        return karma
    except Exception as ex:
        print(f'tried for redditor: {redditor_name}, However, got exception: {ex}')
        return None

if __name__ == '__main__':
    # data['karma'] = data['author'].swifter.apply(get_karma)
    with mp.Pool(mp.cpu_count()) as pool:
        # data['karma'] = data['author'].apply(get_karma)
        data['karma'] = pool.map(get_karma, data['author'])
    data.to_sql('reddit_user_karma', con=conn, if_exists='append')