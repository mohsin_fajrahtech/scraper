import traceback
from pytrends.request import TrendReq
from datetime import datetime, timedelta
import os
import pandas as pd
import requests
import psycopg2
import boto3
import time
import itertools

AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"
last_execution_date = ""
filter_number = 144

folderpath = 'google_trends/googletrends/'+datetime.today().strftime("%Y-%m-%d")

date = datetime.now()
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")


def get_last_execution_date():
	global last_execution_date
	cur = conn.cursor()
	cur.execute("select execution_end_time from quantflare_logs where script_name = 'google_trends' and execution_status = 'Completed' order by id desc limit 1")
	for row in cur:
		last_execution_date = row[0]
	if not last_execution_date:
		last_execution_date = datetime.utcnow()
	cur.close()


get_last_execution_date()
start_date = datetime.utcnow() - timedelta(days=1) #+ timedelta(hours=5)
#end_date = datetime.today() - timedelta(days=1) + timedelta(hours=5)
#end_date = datetime.today() - timedelta(days=0) 
end_date = datetime.utcnow()
py_end_date = datetime.utcnow()
#py_end_date = datetime.today()

start_date = start_date.replace(hour=00, minute=00)
end_date = end_date.replace(hour=00, minute=00)

print ((last_execution_date-start_date).days)


filter_number = filter_number - (24 * (7 - (last_execution_date-start_date).days))
print(filter_number)
print(start_date)
print(end_date)
print(py_end_date)


datelist = []
coinlist = []
coinlistsymbol = []

execution_date = str(datetime.now().strftime('%Y-%m-%d'))
log_id = 0
row_count = 0
coin_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0



def log_step(stepname):
	global conn
	global log_id
	global row_count
	global execution_date
	global start_timestamp
	global end_timestamp
	global exception_text
	global previous_count
	global percent_change


	cur = conn.cursor()
	if(stepname == 'start'):
		start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
			 VALUES(%s,%s,%s,%s) RETURNING id;"""
		start_timestamp = datetime.now()
		cur.execute(start_sql, ("google_trends","In Progress", execution_date, str(start_timestamp)))
		log_id = cur.fetchone()[0]
		conn.commit()
		cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'google_trends' order by row_number desc ) as rows where row_number = 1 ")
		for row in cur:
			previous_count = int(row[1])
		cur.close()

	if(stepname == 'end'):
		end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
		inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
		WHERE id = %s 
		"""
		end_timestamp = datetime.now()
		if(previous_count != 0):
			percent_change = ((row_count-previous_count)/(previous_count))*100
		else:
			percent_change = None
		seconds = (end_timestamp - start_timestamp).total_seconds()
		hours = seconds/3600
		cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text,percent_change, log_id))
		conn.commit()
		cur.close()
		conn.close()

	if(stepname == 'error'):
		end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
		inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s, percentage_diff = %s
		WHERE id = %s 
		"""
		end_timestamp = datetime.now()
		if(previous_count != 0):
			percent_change = ((row_count-previous_count)/(previous_count))*100
		else:
			percent_change = None
		seconds = (end_timestamp - start_timestamp).total_seconds()
		hours = seconds/3600
		cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text, percent_change, log_id))
		conn.commit()
		cur.close()
		exit()



def load_data_db(filepath):
	f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")
	cur = conn.cursor()

	copy = "COPY googletrends(trendvalue,trendtimestamp,coin) FROM STDIN with csv HEADER"
	cur.copy_expert(sql=copy, file=f_cm)

	conn.commit()
	cur.close()


def upload_file(filepath):
	s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
								aws_secret_access_key=AWS_SECRET)

	s3.upload_file(filepath, BUCKET, filepath)


def daterange(start_date, end_date):
	delta = timedelta(hours=1)
	while start_date < end_date:
		yield start_date
		start_date += delta


log_step("start")

counter = 0

for single_date in daterange(start_date, end_date):
	if(counter < 170):
		datelist.append(single_date.strftime("%Y-%m-%d %H:%M"))
		counter += 1

proxieslist = ['https://173.208.46.156:3128','https://50.2.25.137:3128','https://50.2.25.114:3128','https://173.234.248.70:3128','https://173.234.165.184:3128','https://173.208.46.170:3128','https://173.234.165.118:3128','https://173.234.165.163:3128','https://173.234.165.32:3128','https://173.234.248.58:3128']
modulusiterator = 0
m = 0

print(datelist)
print(len(datelist))

coin_length = 300
top_coin_response = ''
url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=505b1c4f-f05a-43d5-8a7a-348d72381790&sort=market_cap&start=1&limit={}&sort_dir=desc".format(coin_length)
try:
	get_top_coins = requests.get(url)
	top_coin_response = get_top_coins.json()['data']
except Exception as ex:
	exception_text += str(ex.args[0])
	print(exception_text)
	log_step("error")

for coin in top_coin_response:
	coinlist.append(coin['name'])
	coinlistsymbol.append(coin['symbol'])

targetcoin = []

try:
	for coin,symbol in zip(coinlist,coinlistsymbol):
		targetcoin.append(coin)
		print(symbol)
		m = modulusiterator%10
		#pytrends = TrendReq(hl='en-US', tz=360, proxies = {'https': proxieslist[m]})
		pytrends = TrendReq(hl='en-US', tz=360)
		#print("This proxy is going to be used: " + proxieslist[m])
		df = pytrends.get_historical_interest(targetcoin, year_start=start_date.year, month_start=start_date.month, day_start=start_date.day, hour_start=0, year_end=py_end_date.year, month_end=py_end_date.month, day_end=py_end_date.day, hour_end=0, cat=0, geo='', gprop='', sleep=0)
		print(df)
		if(df.empty):
			print(symbol + " has empty dataframe")
			targetcoin = []
			modulusiterator += 1
		else:
			df['date'] = datelist
			df['coin'] = symbol
			del df['isPartial']
			df = df[filter_number:]
			print(df)
			row_count += 24
			if not os.path.exists(folderpath):
				os.makedirs(folderpath)
			df.to_csv(path_or_buf=folderpath+'/'+ targetcoin[0] +'.csv', mode='w',header=False, sep=',', index=False, encoding="utf-16")
			# upload_file(folderpath + '/' + targetcoin[0] +'.csv')
			# print(targetcoin[0] + " file is saved on s3")
			print(str(row_count) + " current row count")
			load_data_db(f'./{folderpath}/{targetcoin[0]}.csv')
			print(targetcoin[0] + " file is loaded in RDS")
			targetcoin = []
			if((row_count/24)%10 == 0):
				time.sleep(60)
				print("Going in sleep mode")
			modulusiterator += 1
	log_step("end")
except Exception as ex:
	traceback.print_exc()
	print(str(ex))