def log_step(script_name, stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = datetime.datetime.now()
        cur.execute(start_sql, (script_name,"In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.close()

    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s
        WHERE id = %s 
        """
        end_timestamp = datetime.datetime.now()
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, log_id))
        conn.commit()
        cur.close()
        conn.close()
