import tweepy
import requests
import datetime
import pandas as pd
from time import sleep
import boto3
import time
import json
import os
import csv
import psycopg2
from textblob import TextBlob

from settings import CONSUMER_KEY_STOCKS, CONSUMER_SECRET_STOCKS
from settings import ACCESS_TOKEN_STOCKS, ACCESS_TOKEN_SECRET_STOCKS, AWS_KEY, AWS_SECRET, REGION, BUCKET, ALTCOINS

print("start time: " + str(datetime.datetime.now()))

execution_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
log_id = 0
row_count = 0
start_timestamp = datetime.datetime.now()
end_timestamp = datetime.datetime.now()
exception_text = ""
previous_count = 0
percent_change = 0
start = 1
interval = 100
coin_list = ""

last_execution_date = "2019-01-01"

def get_last_execution_date():
    global last_execution_date
    cur = conn.cursor()
    cur.execute("select date(execution_end_time) from quantflare_logs where script_name = 'twitter_stocks' and execution_status = 'Completed' order by id desc limit 1")
    for row in cur:
        last_execution_date = row[0]
    cur.close()


def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = datetime.datetime.now()
        cur.execute(start_sql, ("twitter_stocks","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'twitter_stocks' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])

        cur.close()

    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s,  percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = datetime.datetime.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text,percent_change, log_id))
        conn.commit()
        cur.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s,  percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = datetime.datetime.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()
        curr.close()



def upload_file(filepath):
    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    #filenameWithPath = 'twitter/'+datetime.date.today().strftime("%Y-%m-%d")+'/'+ coinname +'.csv'                            
    #path_filename= 'twitter/'+datetime.date.today().strftime("%Y-%m-%d")+'/'+ coinname +'.csv'

    s3.upload_file(filepath, BUCKET, filepath)

def load_data_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf16",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='coin-market.cuovmdkoy4e4.us-east-1.rds.amazonaws.com' password='Db1234!.'")
    cur = conn.cursor()


    copy = "COPY twitter_stocks(tweet_date,stock,tweet_text,retweet_text,raw_tweet,polarity,subjectivity) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()

def create_twitter_connection():
    """Create connection with the credentials from settings.py
        and return the api."""

    auth = tweepy.OAuthHandler(CONSUMER_KEY_STOCKS, CONSUMER_SECRET_STOCKS)
    auth.set_access_token(ACCESS_TOKEN_STOCKS, ACCESS_TOKEN_SECRET_STOCKS)

    return tweepy.API(auth, wait_on_rate_limit=True)


def get_date(days_to_look_back: int=1):
    """
    Get the dates for which to gather the tweets.
    """
    if days_to_look_back > 9 or days_to_look_back < 1:
        raise ValueError('ERROR: You can only get 9 days old tweets')

    # Get the previous date
    dt_to = datetime.date.today() #20
    dt_from = datetime.datetime.strptime(str(last_execution_date), '%Y-%m-%d').date() - datetime.timedelta(days=days_to_look_back)
    print(dt_to)
    print(dt_from)

    return dt_from, dt_to


def get_text(tweet):
    """Get the full text of a tweet"""
    # If there is a retweet, get the text
    try:
        retweet_text = tweet['retweeted_status']['full_text']
    except KeyError as e:
        retweet_text = None

    text = tweet['full_text']

    return text, retweet_text


def get_tweets(coin, folderpath,  hashtags: list, days_to_look_back: int=1):
    """
    Retrieve tweets from the day that specified by the days_to_look_back
    :param: days_to_look_back: Number that indicates how many days to look back
    Return: df: tweets
    """
    global row_count
    dt_from, dt_to = get_date(days_to_look_back=days_to_look_back)
    API = create_twitter_connection()
    format = '%a %b %d %H:%M:%S %z %Y'
    row_list = list()

    for hashtag in hashtags:
        for page in tweepy.Cursor(API.search, q=hashtag, count=100,
                                  since=dt_from, until=dt_to,
                                  tweet_mode='extended').pages():

            for tweet in page:
                raw_tweet = tweet._json
                timestamp = raw_tweet['created_at']
                timestamp = datetime.datetime.strptime(timestamp, format)


                if raw_tweet is None:
                    raw_tweet = ""

                text, retweet_text = get_text(raw_tweet)
                if retweet_text is None:
                    retweet_text = ""
                if text is None:
                    text = ""
                sentimented = TextBlob(text)
                temp_dict = {'timestamp': timestamp.strftime('%Y-%m-%d %H:%M:%S'),
                             'stock': hashtag[1:],
                             'text': text.replace('\n', "") ,
                             'retweet_text': retweet_text.replace('\n', ""),
                             'raw_tweet': raw_tweet,
                             'polarity':sentimented.sentiment.polarity,
                             'subjectivity': sentimented.sentiment.subjectivity
                             }
                
                #print(temp_dict)
                row_list.append(temp_dict)
            sleep(3)
    
    if row_list: 
        keys = row_list[0].keys()
        if not os.path.exists(folderpath):
            os.makedirs(folderpath)
        # output = csv.DictWriter(open(folderpath+'/'+ hashtag +'.csv', 'w+',encoding="utf-8"), delimiter=',', lineterminator='\n', fieldnames=keys)
        # output.writerow(dict((fn,fn) for fn in headers))
        # for row in rows:
        #     output.writerow(row)
        with open(folderpath+'/'+ hashtag +'.csv', 'w+',encoding="utf-16",newline='') as output_file:
            dict_writer = csv.DictWriter(output_file,keys,quotechar = '"')
            dict_writer.writeheader()
            #output.writerow(dict((fn,fn) for fn in headers))
            #dict_writer.writerow(dict((fn,fn) for fn in keys))
            #output.writerows(rows)
            dict_writer.writerows(row_list)
            row_count += len(row_list)
            
        upload_file(folderpath+'/'+ hashtag +'.csv')
        load_data_db(folderpath+'/'+ hashtag +'.csv')        
        # tweet_df = pd.DataFrame(row_list)
        # tweet_df.drop_duplicates(subset=['text'], inplace=True)

        #return tweet_df
    else:
        print('no tweets found for '+hashtag)


def get_altcoins(client, altcoin_names):
    """
    Retrieve the ids of the coins for faster indexing
    """
    if not isinstance(altcoin_names, list):
        altcoin_names = [altcoin_names]

    collection = client.tweets.altcoins
    altcoins = collection.find({'name': {'$in': altcoin_names}})

    return list(altcoins)


def main():
    get_last_execution_date()

    # Give a list of tweet names, get their ids
    stock_list = ALTCOINS
    print(stock_list)
    folderpath = 'twitter_stock/'+datetime.date.today().strftime("%Y-%m-%d")
    #client = pymongo.MongoClient()
    #altcoins = get_altcoins(client, altcoin_names)
    global exception_text

    log_step("start")


    for stock in stock_list:
        print("Getting tweets for stock :" +stock[0])
        tweet_df = get_tweets(coin='test',
                                hashtags=stock,
                                days_to_look_back=1,folderpath = folderpath)
        print("Saved tweets for stock :" +stock[0])
        #print('Gathered {0} of filter {1} tweets'.format(len(tweet_df),
  #                                                       altcoin['name']))

    #client.tweets.tweets2.insert_many(tweet_df.to_dict("records"))
    
    log_step("end")
    print("end time: " + str(datetime.datetime.now()))



if __name__ == '__main__':
    main()
