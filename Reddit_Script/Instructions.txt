1. Make sure you have python 3.x installed on your pc
2. Open command line in the folder where the scripts are located (This can be done by Holding SHIFT + RIGHT CLICK)
3. Type "pip3 install -r requirements.txt" so it installs the packages needed for the script to run
4. Still in the command line run "python3 GrabTop100.py" which grabs the top 100 cryptocurrencies off of coinmarketcap's website and generates a file called 'media_handles.json
	(You only need to run step 4 when you want to update the top100 list)
5. In the command line again run "python3 scrape.py" to run the scraper
