import json 
import os
import csv
import praw
from praw.models import MoreComments
import pandas as pd
import datetime as dt
import datetime
import time
import re
import string
from pathlib import Path
import boto3
import psycopg2
import multiprocessing.dummy as mp 
import requests
from time import sleep
from textblob import TextBlob
import traceback

#Reads json file that grabs the top 100 currency
#json_data = open('Reddit_Script/media_handles.json').read()
#data = json.loads(json_data)

AWS_KEY="AKIAIOTVGAYTSJCJUDWQ"
AWS_SECRET="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"
REGION="us-east-1"
BUCKET = "coin-maker-cap-raw"

names = []
reddit_links = []
now = dt.datetime.now()
date_folder = datetime.date.today().strftime("%Y-%m-%d")
coins = {}

execution_date = str(dt.datetime.now().strftime('%Y-%m-%d'))
conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
log_id = 0
row_count = 0
start_timestamp = ""
end_timestamp = ""
exception_text = ""
previous_count = 0
percent_change = 0
start = 1
interval = 100

last_execution_date = ""

def get_last_execution_date():
	global last_execution_date
	cur = conn.cursor()
	cur.execute("select execution_end_time from quantflare_logs where script_name = 'reddit' and execution_status = 'Completed' order by id desc limit 1")
	for row in cur:
		last_execution_date = row[0]
	if not last_execution_date:
		last_execution_date = datetime.datetime.utcnow()
	cur.close()

def log_step(stepname):
    global conn
    global log_id
    global row_count
    global execution_date
    global start_timestamp
    global end_timestamp
    global exception_text
    global previous_count
    global percent_change


    cur = conn.cursor()
    if(stepname == 'start'):
        start_sql = """INSERT INTO quantflare_logs(script_name, execution_status, date_of_execution, execution_start_time)
             VALUES(%s,%s,%s,%s) RETURNING id;"""
        start_timestamp = dt.datetime.now()
        cur.execute(start_sql, ("reddit","In Progress", execution_date, str(start_timestamp)))
        log_id = cur.fetchone()[0]
        conn.commit()
        cur.execute("select script_name, inserted_row_count from (select script_name, execution_status, inserted_row_count, row_number() over (partition by script_name order by execution_start_time desc) as row_number  from quantflare_logs where execution_status = 'Completed' and script_name = 'reddit' order by row_number desc ) as rows where row_number = 1 ")
        for row in cur:
            previous_count = int(row[1])

        cur.close()

    if(stepname == 'end'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s,  percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.datetime.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        print(end_timestamp)
        print(start_timestamp)
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Completed",exception_text, percent_change, log_id))
        conn.commit()
        cur.close()
        conn.close()

    if(stepname == 'error'):
        end_sql = """UPDATE quantflare_logs set execution_time_hours = %s, 
        inserted_row_count = %s, execution_end_time = %s, execution_status = %s, exception_text = %s,  percentage_diff = %s
        WHERE id = %s 
        """
        end_timestamp = dt.datetime.now()
        if(previous_count != 0):
            percent_change = ((row_count-previous_count)/(previous_count))*100
        else:
            percent_change = None
        seconds = (end_timestamp - start_timestamp).total_seconds()
        hours = seconds/3600
        cur.execute(end_sql, (hours, row_count, end_timestamp, "Failed",exception_text,percent_change, log_id))
        conn.commit()
        cur.close()


#db inserts
def load_data_reddit_posts_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()
    copy = "COPY reddit_post(reddit_name,coin,title,post_id,score,created,polarity,subjectivity) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()
    conn.close()

def load_data_reddit_comments_db(filepath):
    f_cm = open(filepath, 'r',encoding="utf8",newline="\r\n")

    conn = psycopg2.connect("dbname='coinmarket' user= 'root' host='0.0.0.0' password='root'")
    cur = conn.cursor()
    copy = "COPY reddit_comment(reddit_name,coin,comment,post_id,score,created,polarity,subjectivity) FROM STDIN with csv HEADER"
    cur.copy_expert(sql=copy, file=f_cm)

    conn.commit()
    cur.close()
    conn.close()

#upload file to s3
def upload_file(filepath):
    s3 = boto3.client('s3', aws_access_key_id=AWS_KEY,
                                aws_secret_access_key=AWS_SECRET)

                                
    #filenameWithPath = 'twitter/'+datetime.date.today().strftime("%Y-%m-%d")+'/'+ coinname +'.csv'                            
    #path_filename= 'twitter/'+datetime.date.today().strftime("%Y-%m-%d")+'/'+ coinname +'.csv'

    s3.upload_file(filepath, BUCKET, filepath)

#Login information for reddit
reddit = praw.Reddit(client_id='p6lMFlg6H_slag', \
                     client_secret='MgWX58Ft7Yrms-porlFrjANPlqs', \
                     user_agent='ascript', \
                     username='justforthescriptslol', \
                     password='justforthescriptslol')

#Fixes date and time for csv files
def get_date(created):
    return dt.datetime.fromtimestamp(created)

#Removes punctuations so that it is removed when naming files
def removePunc(word):
    return re.sub(r'[^\w\s]','',word)

#Sends subReddit name to parameter and grabs each post from each subReddit
def grabPosts(subReddit_name):
    try:
        
        global row_count
        global exception_text
        global last_execution_date
        current_timestamp = time.time()
        #end_time = current_timestamp - (60 * 60 * 24 * 1)
        #start_time = current_timestamp - (60 * 60 * 24 * 2)
        
        #query = 'timestamp:{}..{}'.format(end_time, start_time)
        #print(query)
        subreddit = reddit.subreddit(subReddit_name)
        hot_python = subreddit.top(time_filter='week', limit=None)
        #hot_python = subreddit.hot(limit=None)
        #hot_python = subreddit.search(query, sort='new')
        topics_dict = { 
                        "sub_reddit_name":[], \
                        "coin" : [], \
                        "title":[], \
                        "id":[], \
                        "score":[], \
                        "created":[], \
                        "polarity":[], \
                        "subjectivity":[], \
                    }

        for submission in hot_python:
  
            if(get_date(submission.created) > last_execution_date):

                title_blob = TextBlob(submission.title)

                topics_dict['sub_reddit_name'].append(subReddit_name)
                topics_dict['coin'].append(coins[subReddit_name])
                topics_dict['title'].append(submission.title)
                topics_dict['id'].append(submission.id)
                topics_dict['score'].append(submission.score)
                topics_dict['created'].append(get_date(submission.created))
                topics_dict['polarity'].append(title_blob.sentiment.polarity)
                topics_dict['subjectivity'].append(title_blob.sentiment.subjectivity)
                topics_data = pd.DataFrame(topics_dict)
        if 'topics_data' in locals():
            row_count += len(topics_data['id']) 
        else:
            topics_data = pd.DataFrame(topics_dict)
                



        #print(len(topics_data['id']) )
            #   _timestamp = topics_data["created"].apply(get_date)
            #   topics_data = topics_data.assign(timestamp = _timestamp)
        path = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'posts.csv')
        topics_data.to_csv(path,index=False)
        # upload_file('reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'posts.csv')
        load_data_reddit_posts_db('reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'posts.csv')
        print('saved to ' + path)
        topics_dict.clear()
        #sleep(5)
    except Exception as ex:
       traceback.print_exc()
       print(ex)
       exception_text += str(ex) + " "
   

#Grabs ID's from the posts csv file from each currency and grabs every comment in the subreddit
def grabComments(subReddit_name):
    try:
        global last_execution_date
        print(subReddit_name)
        global row_count
        global exception_text
        current_timestamp = time.time()
        end_time = current_timestamp - (60 * 60 * 24 * 1)
        #start_time = current_timestamp - (60 * 60 * 24 * 2)

        pathP = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'posts.csv')
        pathC = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'comments.csv')
        thread_id = []

        comment_dict = {   "sub_reddit_name":[], \
                            "coin" : [], \
                            "comment":[], \
                            "id":[], \
                            "score":[], \
                            "created":[], \
                            "polarity":[], \
                            "subjectivity":[], \

                        }
        with open(pathP, mode='r', encoding='utf-8', errors='ignore') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                thread_id.append(row['id'])
        post_id = []
        #To make the utf-8 encoding readable to PRAW
        cur = conn.cursor()
        cur.execute("""SELECT distinct post_id from public.reddit_post where reddit_name = %s  limit 500;""",(subReddit_name,))
        print("The number of parts: ", cur.rowcount)
        row = cur.fetchone()
        while row is not None:
            post_id.append(row[0])
            row = cur.fetchone()
        cur.close()
        thread_id = [s[0:8] for s in thread_id]
        thread_id = thread_id + post_id

        #Runs through every subreddit thread, and grabs comments from each thread
        for thread in thread_id:
            comment = reddit.submission(id=thread)
            comment.comment_sort = 'new'
            for top_level_comment in comment.comments:
                try:
                    if get_date(top_level_comment.created) > last_execution_date :
                        comment_blob = TextBlob(str(top_level_comment.body.encode('utf-8')))
                        comment_dict['sub_reddit_name'].append(subReddit_name)
                        comment_dict['coin'].append(coins[subReddit_name])
                        comment_dict['comment'].append(top_level_comment.body.encode('utf-8'))
                        comment_dict['score'].append(top_level_comment.score)            
                        comment_dict['id'].append(thread)
                        comment_dict['created'].append(get_date(top_level_comment.created))
                        comment_dict['polarity'].append(str(comment_blob.sentiment.polarity))
                        comment_dict['subjectivity'].append(str(comment_blob.sentiment.subjectivity))
                        try:
                            comment_data = pd.DataFrame(comment_dict)
                        except Exception as ex:
                            print(ex)
                            exception_text += str(ex) + " "
                            pass 
                        if isinstance(top_level_comment, MoreComments):
                            continue
                        # comment_dict['comment'].append(top_level_comment.body.encode('utf-8'))
                        # comment_dict['id'].append(thread)
                        # comment_dict['created'].append(top_level_comment.created)
                        # comment_data = pd.DataFrame(comment_dict)
                except(AttributeError):
                    print('Comment doesnt exist')
                    pass
        try:
            comment_data.to_csv(pathC,index=False)
            # upload_file('reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'comments.csv')
            load_data_reddit_comments_db('reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'comments.csv')
        
            print('saved to ' + pathC)
            row_count += len(comment_dict['id'])
        except Exception as ex:
            print(ex)
            exception_text += str(ex) + " "
        
        comment_dict.clear()
    except Exception as ex:
        print(ex)
        exception_text += str(ex) + " "

#Grabs Volumes from each subreddit and places them in volume folder
def grabVolume(subReddit_name):
   volume_dict = { 
                   "sub_reddit_name": [], \
                   "post":[], \
                   "num_comments":[], \
                   "date_time":[]
               }
   volume_dict2 = {
                   "sub_reddit_name": [], \
                   "post2":[], \
                   "num_comments":[], \
                   "date_time":[]
               }

   pathP = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/' + 'posts.csv')
   punctuation = string.punctuation
   thread_id = []
   #Gets Thread ID from csv file
   with open(pathP, mode='r', encoding='utf-8', errors='ignore') as csv_file:
       reader = csv.DictReader(csv_file)
       for row in reader:
          thread_id.append(row['id'])
       
   #Removes some encoding from 'utf-8'
   thread_id = [s[0:8] for s in thread_id]

   count = 0
   for thread in thread_id:
      post = reddit.submission(id=thread)
      
      volume_dict["sub_reddit_name"].append(subReddit_name)      
      volume_dict["post"].append(post.title.encode('utf-8'))
      volume_dict["num_comments"].append(post.num_comments)
      volume_dict["date_time"].append(now.strftime("%Y-%m-%d %H:%M"))
      #print(volume_dict["post"][count])
      count += 1


   volume_dict['post'] = [item.decode('utf-8').strip() for item in volume_dict['post']]

   #Removes punctuation and puts it into a new list
   newlist = []
   for word in volume_dict['post']:
     newlist.append(removePunc(word))
   #newlist = [item.decode('ascii') for item in newlist]
   newlist = [word_length[:250] for word_length in newlist]
   count = 0
   for post in newlist:
      volume_dict2["sub_reddit_name"].append(subReddit_name)
      volume_dict2['post2'].append(post)
      volume_dict2['num_comments'].append(volume_dict['num_comments'][count])
      volume_dict2['date_time'].append(now.strftime("%Y-%m-%d %H:%M"))
      pathV = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits' + '/' + subReddit_name + '/Volumes' + '/' + post + '.csv')
      my_file = Path(pathV)
      if my_file.is_file:
         volume_data = pd.DataFrame(volume_dict2)
         with open(pathV, 'a', encoding='utf-8') as fd:
           volume_data.to_csv(fd, index=False, header=False, encoding='utf-8')
           del volume_dict2['post2'][0]
           del volume_dict2['num_comments'][0]
           del volume_dict2['date_time'][0]
      else:
         volume_data = pd.DataFrame(volume_dict2)
         volume_data.to_csv(pathV, index=False, header=True, encoding='utf-8')
         #print('saved to' + pathV)
         del volume_dict2['post2'][0]
         del volume_dict2['num_comments'][0]
         del volume_dict2['date_time'][0]
      count += 1

#Generates folders based on subReddit names
def createFolder(dir):
   print(dir)
   try:
      if not os.path.exists('subReddits'):
         os.makedirs('subReddits')
   except:
         print('Directory does not exist now creating: ' + dir + ' folder')
   for subfolder_names in names:
      try:
         if not os.path.exists(dir):
            os.makedirs(os.path.join('subReddits', dir))
            print(os.path.join('subReddits', dir))
      except(OSError):
          #print('Folder already exist')
          pass

#Generates Volume Folders
def generateVolumeFolder(dir):
   volumes_path = os.path.join(os.getcwd() +'/reddit/'+date_folder+ '/subReddits/' + dir + '/Volumes')
   try:
      if not os.path.exists(volumes_path):
         os.makedirs('Volumes')
   except:
         print('Directory does not exist now creating: ' + dir + ' volumes folder')
   for subfolder_names in names:
      try:
         if not os.path.exists(dir):
            os.makedirs(volumes_path)
      except(OSError):
         #print('Folder already exist')
         pass

#Main Function
if __name__ == "__main__": 
    coin_list = ""
    get_last_execution_date()
    coin_symbols = []
    for x in range(3):
        coin_list = ""
        url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=505b1c4f-f05a-43d5-8a7a-348d72381790&sort=market_cap&start={}&limit={}&sort_dir=desc".format(start,interval)
        get_top_coins = requests.get(url)
        top_coin_response = get_top_coins.json()['data']
        for coin in top_coin_response:
            coin_list = coin_list + coin['symbol'] + ','
        coin_list = coin_list[:-1]

        repo_url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/info?CMC_PRO_API_KEY=505b1c4f-f05a-43d5-8a7a-348d72381790&symbol={}".format(coin_list)
        #print(repo_url)
        get_top_repo = requests.get(repo_url)
        top_repo_response = get_top_repo.json()['data']
        #print (type(top_coin_response))
        for key, value in top_repo_response.items():
            if len(value['urls']['reddit']) > 0:
                #print(value['urls']['reddit'][0])
                coin_symbols.append(key)
                coins[value['urls']['reddit'][0].replace('.embed?limit=9', "").replace('-', "").replace('https://www.reddit.com/r/', "").replace('https://reddit.com/r/', "")] = key       
            
    #    print(top_repo_response)

        start = start + interval
    print(coins)
   
    reddit_names =  []
    log_step("start")
    for key, value in coins.items():
        reddit_names.append(key)

#     names = [item.replace('-', "") for item in names]
#    #Makes it easier to name folders and have accuracy with the subreddit names and currency

#     reddit_links = [item.replace('.embed?limit=9', "") for item in reddit_links]
#     reddit_links = [item.replace('-', "") for item in reddit_links]
#     reddit_names = [item.replace('https://www.reddit.com/r/', "") for item in reddit_links]

   #For creating folders and grabbing posts
    print('STATUS: Creating Folders and Grabbing Posts')
    t = reddit_names
    names = t
    print(t)
    for name in t:
       createFolder(name)
       generateVolumeFolder(name)

    p=mp.Pool(20)
    p.map(grabPosts,t) # range(0,1000) if you want to replicate your example

    p.close()
    p.join()         
    print('STATUS: Grabbing comments from each subreddit')
    p=mp.Pool(20)
    p.map(grabComments,t) # range(0,1000) if you want to replicate your example
    p.close()
    p.join()
#    for name in t:
#       grabComments(name)
#   print('STATUS: Populating volumes folder ')
#    for name in t:
#       print(name)
#       grabVolume(name)
    log_step("end")