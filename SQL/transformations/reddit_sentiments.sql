DROP TABLE IF EXISTS reddit_sentiments;
CREATE TABLE reddit_sentiments
AS
WITH reddit_post AS (
    SELECT epoch,
           stamp,
           coin,
           AVG(score) AS avg_post_vote,
           SUM(CASE
                    WHEN reddit_pushshift.polarity > 0.2 and reddit_pushshift.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) positive_posts,
           SUM(CASE
                    WHEN reddit_pushshift.polarity < -0.1 and reddit_pushshift.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) negative_posts
    FROM reddit_pushshift
    GROUP BY 1,2,3
),
reddit_comments AS (
    SELECT epoch,
           stamp,
           coin,
           AVG(score) AS avg_comment_vote,
           SUM(CASE
                    WHEN reddit_pushshift_comments.polarity > 0.2 and reddit_pushshift_comments.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) positive_comments,
           SUM(CASE
                    WHEN reddit_pushshift_comments.polarity < -0.1 and reddit_pushshift_comments.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) negative_comments
    FROM reddit_pushshift_comments
    GROUP BY 1,2,3
)
SELECT epoch,
       coin,
       reddit_post.stamp,
       reddit_post.avg_post_vote,
       reddit_post.positive_posts,
       reddit_post.negative_posts,
       COALESCE(reddit_comments.avg_comment_vote,0) AS avg_comment_vote,
       COALESCE(reddit_comments.positive_comments,0) AS positive_comments,
       COALESCE(reddit_comments.negative_comments,0) AS negative_comments
FROM reddit_post
LEFT JOIN reddit_comments USING (epoch, coin)
ORDER BY reddit_post.stamp ASC;