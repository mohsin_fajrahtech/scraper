DROP TABLE IF EXISTS reddit_sentiments_vader;
CREATE TABLE reddit_sentiments_vader
AS
WITH reddit_post AS (
    SELECT epoch,
           stamp,
           coin,
           AVG(ruk.karma) as avg_post_karma,
           AVG(reddit_pushshift_vader.score) AS avg_post_vote,
           AVG(reddit_pushshift_vader.compound) as avg_post_compound
    FROM reddit_pushshift_vader
    LEFT JOIN reddit_user_karma ruk on reddit_pushshift_vader.author = ruk.author
    GROUP BY 1,2,3
),
reddit_comments AS (
    SELECT epoch,
           stamp,
           coin,
           AVG(r.karma) as avg_comment_karma,
           AVG(reddit_pushshift_comments_vader.score) AS avg_comment_vote,
           AVG(reddit_pushshift_comments_vader.compound) as avg_comment_compound
    FROM reddit_pushshift_comments_vader
    LEFT JOIN reddit_user_karma r on reddit_pushshift_comments_vader.author = r.author
    GROUP BY 1,2,3
)
SELECT epoch,
       coin,
       reddit_post.stamp,
       reddit_post.avg_post_karma,
       reddit_post.avg_post_vote,
       reddit_post.avg_post_compound,
       COALESCE(reddit_comments.avg_comment_karma,0) as avg_comment_karma,
       COALESCE(reddit_comments.avg_comment_vote,0) AS avg_comment_vote,
       COALESCE(reddit_comments.avg_comment_compound,0) AS avg_comment_compound
FROM reddit_post
LEFT JOIN reddit_comments USING (epoch, coin)
ORDER BY reddit_post.stamp ASC;