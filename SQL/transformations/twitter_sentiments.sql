DROP TABLE IF EXISTS twitter_sentiments;
CREATE TABLE twitter_sentiments
AS
WITH twitter_post AS (
    SELECT DATE_TRUNC('hour', date) as stamp,
           extract(epoch from DATE_TRUNC('hour', date))  AS epoch,
           'BTC' AS coin,
           SUM(CASE
                    WHEN bitcoin_transformed.polarity > 0.2 and bitcoin_transformed.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) positive_posts,
           SUM(CASE
                    WHEN bitcoin_transformed.polarity < -0.1 and bitcoin_transformed.subjectivity > 0.2
                        THEN 1
                        ELSE 0
                END) negative_posts
    FROM bitcoin_transformed
    GROUP BY 1,2,3
)
SELECT epoch,
       coin,
       twitter_post.stamp,
       twitter_post.positive_posts,
       twitter_post.negative_posts
FROM twitter_post
ORDER BY twitter_post.stamp ASC;