DROP TABLE IF EXISTS reddit_sentiments_no_agg_classification;
CREATE TABLE reddit_sentiments_no_agg_classification
AS
WITH close_cte AS (
    SELECT ohlcv.epoch,
        ohlcv.stamp,
        ohlcv.coin,
        ohlcv.close,
        LAG(ohlcv.close,1) OVER ( ORDER BY ohlcv.stamp ASC) previous_hour_close
    FROM coin_data_ohlcv ohlcv
    WHERE CAST(ohlcv.stamp AS DATE) >= '2021-04-01' and CAST(ohlcv.stamp AS DATE) <= '2021-06-20'
    AND ohlcv.coin = 'BTC'

)
SELECT
    sentiments.stamp,
    sentiments.coin,
    ruk.karma,
    polarity,
    subjectivity,
    compound,
    CASE
        WHEN cte.close - cte.previous_hour_close > 0
            THEN 1
        ELSE 0
    END AS class
FROM reddit_pushshift_vader AS sentiments
LEFT JOIN close_cte AS cte ON sentiments.coin = cte.coin
    AND cte.epoch = sentiments.epoch
LEFT JOIN reddit_user_karma ruk ON sentiments.author = ruk.author
WHERE cte.stamp IS NOT NULL
ORDER BY stamp ASC;
