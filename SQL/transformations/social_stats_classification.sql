DROP TABLE IF EXISTS social_stats_classification;
CREATE TABLE social_stats_classification
AS
WITH close_cte AS (
    SELECT ohlcv.epoch,
        ohlcv.stamp,
        ohlcv.coin,
        ohlcv.close,
        LAG(ohlcv.close,1) OVER ( ORDER BY ohlcv.stamp ASC) previous_hour_close
    FROM coin_data_ohlcv ohlcv
    WHERE CAST(ohlcv.stamp AS DATE) >= '2019-01-01' and CAST(ohlcv.stamp AS DATE) <= '2021-06-10'
    AND ohlcv.coin = 'BTC'

),
social_cte AS (
    SELECT social.epoch,
        social.stamp,
        social.coin,
        social.comments,
        LAG(social.comments,1) OVER ( ORDER BY social.stamp ASC) previous_hour_comments,
        social.posts,
        LAG(social.posts,1) OVER ( ORDER BY social.stamp ASC) previous_hour_posts,
        social.followers,
        LAG(social.followers,1) OVER ( ORDER BY social.stamp ASC) previous_hour_followers,
        social.points,
        social.overview_page_views,
        LAG(social.overview_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_overview_page_views,
        social.total_page_views,
        LAG(social.total_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_total_page_views,
        social.influence_page_views,
        LAG(social.influence_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_influence_page_views,
        social.twitter_following,
        LAG(social.twitter_following,1) OVER ( ORDER BY social.stamp ASC) previous_hour_twitter_following,
        social.fb_likes,
        LAG(social.fb_likes,1) OVER ( ORDER BY social.stamp ASC) previous_hour_fb_likes,
        social.trades_page_views,
        LAG(social.trades_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_trades_page_views,
        social.twitter_favourites,
        LAG(social.twitter_favourites,1) OVER ( ORDER BY social.stamp ASC) previous_hour_twitter_favourites,
        social.forum_page_views,
        LAG(social.forum_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_forum_page_views,
        social.twitter_lists,
        LAG(social.twitter_lists,1) OVER ( ORDER BY social.stamp ASC) previous_hour_twitter_lists,
        social.twitter_statuses,
        LAG(social.twitter_statuses,1) OVER ( ORDER BY social.stamp ASC) previous_hour_twitter_statuses,
        social.fb_talking_about,
        LAG(social.fb_talking_about,1) OVER ( ORDER BY social.stamp ASC) previous_hour_fb_talking_about,
        social.twitter_followers,
        LAG(social.twitter_followers,1) OVER ( ORDER BY social.stamp ASC) previous_hour_twitter_followers,
        social.reddit_posts_per_hour,
        social.reddit_active_users,
        social.markets_page_views,
        LAG(social.markets_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_markets_page_views,
        social.analysis_page_views,
        LAG(social.analysis_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_analysis_page_views,
        social.charts_page_views,
        LAG(social.charts_page_views,1) OVER ( ORDER BY social.stamp ASC) previous_hour_charts_page_views,
        social.reddit_subscribers,
        LAG(social.reddit_subscribers,1) OVER ( ORDER BY social.stamp ASC) previous_hour_reddit_subscribers,
        social.reddit_comments_per_hour,
        social.reddit_posts_per_day,
        social.reddit_comments_per_day
FROM coin_social AS social
WHERE CAST(social.stamp AS DATE) >= '2019-01-01' and CAST(social.stamp AS DATE) <= '2021-06-10'
    AND social.coin = 'BTC'
)
SELECT
    social.stamp,
    social.coin,
    social.comments - previous_hour_comments AS hourly_comments,
    social.posts - previous_hour_posts AS hourly_posts,
    social.followers - previous_hour_followers AS hourly_followers,
    social.points,
    social.overview_page_views - previous_hour_overview_page_views AS hourly_overview_page_views,
    social.total_page_views - previous_hour_total_page_views AS hourly_total_page_views,
    social.influence_page_views - previous_hour_influence_page_views AS hourly_influence_page_views,
    social.twitter_following - previous_hour_twitter_following AS hourly_twitter_following,
    social.fb_likes - previous_hour_fb_likes AS hourly_fb_likes,
    social.trades_page_views - previous_hour_trades_page_views AS hourly_trades_page_views,
    social.twitter_favourites - previous_hour_twitter_favourites AS hourly_twitter_favourites,
    social.forum_page_views - previous_hour_forum_page_views AS hourly_forum_page_views,
    social.twitter_lists - previous_hour_twitter_lists AS hourly_twitter_lists,
    social.twitter_statuses - previous_hour_twitter_statuses AS hourly_twitter_statuses,
    social.fb_talking_about - previous_hour_fb_talking_about AS hourly_fb_talking_about,
    social.twitter_followers - previous_hour_twitter_followers AS hourly_twitter_followers,
    social.reddit_posts_per_hour,
    social.reddit_active_users,
    social.markets_page_views - previous_hour_markets_page_views AS hourly_markets_page_views,
    social.analysis_page_views - previous_hour_analysis_page_views AS hourly_analysis_page_views,
    social.charts_page_views - previous_hour_charts_page_views AS hourly_charts_page_views,
    social.reddit_subscribers - previous_hour_reddit_subscribers AS hourly_reddit_subscribers,
    social.reddit_comments_per_hour,
    social.reddit_posts_per_day,
    social.reddit_comments_per_day,
    cte.close,
    cte.previous_hour_close,
    cte.close - cte.previous_hour_close AS close_price_difference,
    CASE
        WHEN cte.close - cte.previous_hour_close > 0
            THEN 1
        ELSE 0
    END AS class
FROM social_cte AS social
LEFT JOIN close_cte AS cte ON social.coin = cte.coin
    AND cte.epoch = social.epoch
-- WHERE CAST(social.stamp AS DATE) = '2021-06-1'
--     AND social.coin = 'BTC'
ORDER BY stamp ASC;
