DROP TABLE IF EXISTS  coin_data_ohlcv;
CREATE TABLE coin_data_ohlcv (
    id                          SERIAL PRIMARY KEY,
    epoch                       INT,
    stamp                       TIMESTAMP,
    coin                        VARCHAR(10),
    close                       DECIMAL,
    high                        DECIMAL,
    low                         DECIMAL,
    open                        DECIMAL,
    volumefrom                  DECIMAL,
    volumeto                    DECIMAL
);