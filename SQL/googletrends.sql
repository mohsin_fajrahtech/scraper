DROP TABLE IF EXISTS  googletrends;
CREATE TABLE googletrends (
    trend_id                    SERIAL PRIMARY KEY,
    trendvalue                  INT,
    trendtimestamp              TIMESTAMP,
    coin                        VARCHAR(10)
)