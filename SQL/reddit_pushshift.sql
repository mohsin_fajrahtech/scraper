DROP TABLE IF EXISTS reddit_pushshift;
CREATE TABLE reddit_pushshift (
    id                                      SERIAL PRIMARY KEY,
    reddit_name                             VARCHAR(50),
    epoch                                   INT,
    stamp                                   TIMESTAMP,
    coin                                    VARCHAR(10),
    title                                   VARCHAR(65000),
    post_id                                 VARCHAR(20),
    author                                  VARCHAR(100),
    score                                   NUMERIC,
    polarity                                FLOAT,
    subjectivity                            FLOAT,
    created                                 TIMESTAMP
);


DROP TABLE IF EXISTS reddit_pushshift_comments;
CREATE TABLE reddit_pushshift_comments (
    id                                      SERIAL PRIMARY KEY,
    reddit_name                             VARCHAR(50),
    epoch                                   INT,
    stamp                                   TIMESTAMP,
    coin                                    VARCHAR(10),
    body                                    VARCHAR(65000),
    comment_id                              VARCHAR(20),
    author                                  VARCHAR(100),
    score                                   NUMERIC,
    polarity                                FLOAT,
    subjectivity                            FLOAT,
    created                                 TIMESTAMP
);