DROP TABLE IF EXISTS reddit_comment;
CREATE TABLE reddit_comment (
    reddit_name                 VARCHAR(500),
    coin                        VARCHAR(10),
    comment                     VARCHAR(65000),
    post_id                     VARCHAR(20),
    score                       NUMERIC,
    created                     TIMESTAMP,
    polarity                    NUMERIC,
    subjectivity                NUMERIC

);

DROP TABLE IF EXISTS reddit_post;
CREATE TABLE reddit_post (
    reddit_name                 VARCHAR(500),
    coin                        VARCHAR(10),
    title                       VARCHAR(65000),
    post_id                     VARCHAR(20),
    score                       NUMERIC,
    created                     TIMESTAMP,
    polarity                    NUMERIC,
    subjectivity                NUMERIC

);