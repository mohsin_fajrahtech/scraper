DROP TABLE IF EXISTS quantflare_logs;
CREATE TABLE quantflare_logs (
    id                              SERIAL PRIMARY KEY,
    script_name                     VARCHAR(50),
    execution_status                VARCHAR(50),
    inserted_row_count              INT,
    date_of_execution               DATE,
    execution_time_hours            NUMERIC,
    execution_start_time            TIMESTAMP,
    execution_end_time              TIMESTAMP,
    exception_text                  VARCHAR(250),
    percentage_diff                 NUMERIC
);